import Link from "next/link";

export default function Home() {
  return (
    <main className="h-screen flex flex-col items-center justify-center bg-teal-400 gap-5">
      <Link href="/">
        <section className="text-2xl font-semibold bg-teal-500 p-4 rounded-lg text-zinc-800 hover:bg-teal-600">
          This is the entry point to the Next.js application
        </section>
      </Link>
      <ul className="text-2xl font-semibold">
        {["about", "blogs", "career", "contacts", "projects", "skills"].map(
          (listItem, index) => {
            const itemURL = `/${listItem}`;
            return (
              <li key={index}>
                <Link
                  href={itemURL}
                  className="flex items-center hover:before:content-['->'] hover:text-sky-500"
                >
                  {listItem}
                </Link>
              </li>
            )
          }
        )}
      </ul>
    </main>
  )
}
