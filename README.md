# Next.js Multi-Directory Repository

Welcome to the Next.js Multi-Directory Repository! This repository contains multiple Next.js projects, each organized in its own directory.

## To get started, clone the entire repository

### Follow the instructions below:

1. Clone the repository
```bash
git clone https://github.com/bisw4sh/Next.js-Practice.git
```

2. Get inside the directory
```bash
cd <nextjs-directory-name>
```

3. Install the dependencies
```bash
pnpm install
```

4. Run Development Server
```bash
pnpm run dev
```
<hr>

- For every project they have their own markdown to guide you through.